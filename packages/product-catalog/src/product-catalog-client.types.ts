import { Entry } from 'contentful';

import {
  parsePricingStrategy,
  PricingPlan,
  PricingTier,
  PricingTierPrice
} from './models';

export type ProductContent = {
  readonly name?: string;
  readonly platform?: Entry<PlatformContent>;
  readonly catalogItemId?: string;
  readonly description: string;
  readonly productFamily?: string;
  readonly productType?: string;
  readonly unitLabel?: string;
  readonly billingType?: string;
  readonly vendor?: Entry<VendorContent>;
  readonly parentProducts?: ReadonlyArray<Entry<ProductContent>>;
  readonly defaultPricingPlanMonthly: Entry<PricingPlanContent> | null;
  readonly defaultPricingPlanYearly: Entry<PricingPlanContent> | null;
  readonly defaultStdPricingPlanMonthly: Entry<PricingPlanContent> | null;
  readonly defaultStdPricingPlanYearly: Entry<PricingPlanContent> | null;
  readonly defaultFreePricingPlanMonthly: Entry<PricingPlanContent> | null;
  readonly defaultFreePricingPlanYearly: Entry<PricingPlanContent> | null;
  readonly defaultPremiumPricingPlanMonthly: Entry<PricingPlanContent> | null;
  readonly defaultPremiumPricingPlanYearly: Entry<PricingPlanContent> | null;
  readonly key?: string;
  readonly sourceSequence?: number;
};

export type PlatformContent = {
  readonly name?: string;
};

export type VendorContent = {
  readonly name?: string;
};

export type PricingPlanContent = {
  readonly product?: Entry<ProductContent>;
  readonly periodUnit?: string;
  readonly periodCount?: number;
  readonly status?: string;
  readonly edition?: string;
  readonly configuration: PricingPlanContentConfiguration;
  readonly pricingPlanId?: string;
  readonly sourceSequence?: number;
};

export type PricingPlanContentConfiguration = ReadonlyArray<
  PricingPlanContentConfigurationItem
>;

export type PricingPlanContentConfigurationItem = {
  readonly starter: boolean;
  readonly pricing_strategy: string;
  readonly unit_description: string;
  readonly unit_limit_lower: number | null;
  readonly unit_limit_upper: number | null;
  readonly prices: ReadonlyArray<PricingPlanContentConfigurationItemPrice>;
};

export type PricingPlanContentConfigurationItemPrice = {
  readonly currency: string;
  readonly unit_price: number;
};

export function mapPricingPlanContentEntryToPricingPlan(
  entry: Entry<PricingPlanContent> | null
): PricingPlan | undefined {
  if (entry) {
    return mapPricingPlanContentToPricingPlan(entry.fields);
  } else {
    return undefined;
  }
}

function mapPricingPlanContentToPricingPlan(
  content: PricingPlanContent
): PricingPlan {
  return content.configuration.map(
    mapPricingPlanContentConfigurationItemToPricingTier
  );
}

function mapPricingPlanContentConfigurationItemToPricingTier({
  starter,
  pricing_strategy,
  unit_description,
  unit_limit_lower,
  unit_limit_upper,
  prices
}: PricingPlanContentConfigurationItem): PricingTier {
  return {
    starter,
    pricingStrategy: parsePricingStrategy(pricing_strategy),
    unitDescription: unit_description,
    unitLimitLower: unit_limit_lower,
    unitLimitUpper: unit_limit_upper,
    prices: prices.map(
      mapPricingPlanContentConfigurationItemPriceToPricingTierPrice
    )
  };
}

function mapPricingPlanContentConfigurationItemPriceToPricingTierPrice({
  currency,
  unit_price
}: PricingPlanContentConfigurationItemPrice): PricingTierPrice {
  return {
    currency,
    unitPrice: unit_price
  };
}
