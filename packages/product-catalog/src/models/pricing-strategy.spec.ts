import {
  isPricingStrategy,
  parsePricingStrategy,
  PricingStrategy
} from './pricing-strategy';

const INVALID_PRICING_STRATEGY = 'INVALID_PRICING_STRATEGY';

describe('isPricingStrategy()', () => {
  it('should return true given valid PricingStrategy', () => {
    expect(isPricingStrategy(PricingStrategy.FIXED_COST)).toEqual(true);
    expect(isPricingStrategy(PricingStrategy.MARGINAL_PER_UNIT)).toEqual(true);
    expect(isPricingStrategy(PricingStrategy.PER_UNIT)).toEqual(true);
  });

  it('should return false given invalid PricingStrategy', () => {
    expect(isPricingStrategy(INVALID_PRICING_STRATEGY)).toEqual(false);
  });
});

describe('parsePricingStrategy()', () => {
  it('should throw an error given invalid PricingStrategy', () => {
    expect(() => parsePricingStrategy(INVALID_PRICING_STRATEGY)).toThrowError(
      `${INVALID_PRICING_STRATEGY} is not a valid PricingStrategy`
    );
  });
});
