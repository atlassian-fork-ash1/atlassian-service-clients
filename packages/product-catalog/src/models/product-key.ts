export type ProductKey = string;

export type ProductKeyMap<T> = {
  readonly [productKey: string]: T | undefined;
};
