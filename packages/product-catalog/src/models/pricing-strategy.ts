export enum PricingStrategy {
  FIXED_COST = 'FIXED_COST',
  MARGINAL_PER_UNIT = 'MARGINAL_PER_UNIT',
  PER_UNIT = 'PER_UNIT'
}

export function isPricingStrategy(value: string): value is PricingStrategy {
  switch (value) {
    case PricingStrategy.FIXED_COST:
    case PricingStrategy.MARGINAL_PER_UNIT:
    case PricingStrategy.PER_UNIT:
      return true;
    default:
      return false;
  }
}

export function parsePricingStrategy(value: string): PricingStrategy {
  if (isPricingStrategy(value)) {
    return value;
  } else {
    throw new SyntaxError(`${value} is not a valid PricingStrategy`);
  }
}
