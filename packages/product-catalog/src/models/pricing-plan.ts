import { PricingTier } from './pricing-tier';

export type PricingPlan = ReadonlyArray<PricingTier>;

export type PricingPlans = {
  readonly monthly?: PricingPlan;
  readonly annual?: PricingPlan;
};
