import nock from 'nock';
import { sampleProductEntriesResponse } from '../test/fixtures/contentful';
import { sampleAddonProductEntriesResponse } from '../test/fixtures/contentful-addons';
import { ProductCatalogClient } from './';
import {
  removeEditionSuffix,
  SEARCH_ADDONS_QUERY,
  SELECT_QUERY,
  DEFAULT_HOST
} from './product-catalog-client';

const space = 'some-space';
const environment = 'some-environment';
const accessToken = 'some-access-token';

const createGetPricingQuery = (productKeys: string[]) => ({
  content_type: 'product',
  select: `${SELECT_QUERY},sys`,
  'fields.key': {
    in: [...new Set(productKeys.map(removeEditionSuffix)).values()].join(',')
  }
});

const createSearchAddonsQuery = (
  parentProductKey: string,
  query: string,
  limit: number
) => ({
  content_type: 'product',
  select: `${SEARCH_ADDONS_QUERY},sys`,
  'fields.productType': {
    match: 'ADDON'
  },
  'fields.parentProductName': {
    match: parentProductKey
  },
  'fields.description': {
    match: query
  },
  limit
});

describe('ProductCatalogClient', () => {
  function setup(
    contentfulQuery: any,
    body: any,
    statusCode: number = 200,
    host = DEFAULT_HOST
  ) {
    nock(`https://${host}`, {
      reqheaders: { authorization: `Bearer ${accessToken}` }
    })
      .get(new RegExp(`^/spaces/${space}/environments/${environment}/entries`))
      .query(contentfulQuery)
      .reply(statusCode, body);

    const client = new ProductCatalogClient({
      space,
      environment,
      accessToken,
      host: host === DEFAULT_HOST ? undefined : host
    });

    return client;
  }

  describe('getPricing', () => {
    it('should match snapshot', async () => {
      const productKeys = [
        'jira-software.ondemand',
        'jira-software.ondemand.premium',
        'jira-software.ondemand.free',
        'random_invalid_entry'
      ];
      const client = setup(
        createGetPricingQuery(productKeys),
        sampleProductEntriesResponse,
        undefined,
        "cdn.contentful.com"
      );

      expect(await client.getPricing({ productKeys })).toMatchSnapshot();
    });
  });

  describe('getPricing', () => {
    it('should use default host if none provided', async () => {
      const productKeys = [
        'jira-software.ondemand',
        'jira-software.ondemand.premium',
        'jira-software.ondemand.free',
        'random_invalid_entry'
      ];
      const client = setup(
        createGetPricingQuery(productKeys),
        sampleProductEntriesResponse
      );

      expect(await client.getPricing({ productKeys })).toMatchSnapshot();
    });
  });

  describe('searchAddons', () => {
    it('should match snapshot', async () => {
      const parentProductKey = 'jira-software.data-center';
      const query = 'explainer';
      const limit = 50;
      const client = setup(
        createSearchAddonsQuery(parentProductKey, query, limit),
        sampleAddonProductEntriesResponse
      );

      expect(
        await client
          .searchAddons({ parentProductKey, query, limit })
          .catch(e => console.log(e))
      ).toMatchSnapshot();
    });
  });
});
