import * as core from '@atlassian/asc-core';
import { address, image, internet, name, random } from 'faker';
import fetchMock from 'fetch-mock';
import { OK } from 'http-status-codes';
import nock from 'nock';
import {
  PatchUserProfileRequest,
  PatchUserProfileRequestBody
} from './identity-client.types';
import {
  DEFAULT_IDENTITY_BASE_URL,
  DEFAULT_STARGATE_BASE_URL,
  IdentityClient
} from './index';

const { serializeCookie, UnsucessfulFetchError } = core;

const asap = {
  issuer: 'some-issuer',
  keyId: 'some-key-id',
  privateKey: 'some-private-key'
};
const asapToken = 'some-asap-token';

describe('IdentityClient', () => {
  const baseUrlStargate = 'http://some.url';
  const baseUrlIdentity = 'http://some.url';
  const cloudSessionTokenCookie = {
    name: 'some-cookie-name',
    value: 'some-cookie-value'
  };

  afterEach(() => nock.cleanAll());

  describe('me', () => {
    it('should respond with user profile given valid token', async () => {
      const identityClient = new IdentityClient();
      const response = {
        account_id: random.uuid(),
        name: name.findName(),
        email: internet.email(),
        picture: image.imageUrl(),
        locale: random.locale(),
        zoneinfo: address.country()
      };

      nock(DEFAULT_STARGATE_BASE_URL, {
        reqheaders: { cookie: serializeCookie(cloudSessionTokenCookie) }
      })
        .get('/me')
        .reply(200, response);

      const { account_id, ...others } = response;

      await expect(
        identityClient.me({ cloudSessionTokenCookie })
      ).resolves.toEqual({
        ...others,
        accountId: account_id
      });
    });

    it('should reject with UnsuccessfulFetchError given Unauthorized response', async () => {
      const identityClient = new IdentityClient({ baseUrlStargate });
      const response = {
        code: 401,
        message: 'Unauthorized'
      };

      nock(baseUrlStargate, {
        reqheaders: { cookie: serializeCookie(cloudSessionTokenCookie) }
      })
        .get('/me')
        .reply(401, response);

      await expect(
        identityClient.me({ cloudSessionTokenCookie })
      ).rejects.toEqual(
        new UnsucessfulFetchError({
          url: `${baseUrlStargate}/me`,
          status: 401,
          message: response.message
        })
      );
    });
  });

  describe('permissionsPermitted', () => {
    beforeEach(() =>
      jest.spyOn(core, 'generateAsapToken').mockResolvedValue(asapToken));

    afterEach(() => {
      jest.restoreAllMocks();
    });

    it('should resolve with permitted response given valid token', async () => {
      const identityClient = new IdentityClient();
      const response = random.boolean().toString();

      nock(DEFAULT_IDENTITY_BASE_URL, {
        reqheaders: { authorization: `Bearer ${asapToken}` }
      })
        .get('/v1/permissions/permitted')
        .query(true)
        .reply(200, response);

      await expect(
        identityClient.permissionsPermitted({
          auth: { asap },
          permissionId: random.word(),
          dontRequirePrincipalInSite: random.boolean(),
          principalId: random.uuid(),
          resourceId: random.uuid()
        })
      ).resolves.toEqual(response);
    });

    it('should reject with UnsuccessfulFetchError given Unauthorized response', async () => {
      const identityClient = new IdentityClient({ baseUrlIdentity });
      const response = {
        code: 401,
        message: 'Unauthorized'
      };

      nock(baseUrlIdentity, {
        reqheaders: { authorization: `Bearer ${asapToken}` }
      })
        .get('/v1/permissions/permitted')
        .query(true)
        .reply(401, response);

      await expect(
        identityClient.permissionsPermitted({
          auth: { asap },
          permissionId: random.word(),
          dontRequirePrincipalInSite: random.boolean(),
          principalId: random.uuid(),
          resourceId: random.uuid()
        })
      ).rejects.toEqual(
        new UnsucessfulFetchError({
          url: `${baseUrlIdentity}/v1/permissions/permitted`,
          status: 401,
          message: response.message
        })
      );
    });

    describe('patchUserProfile', () => {
      afterEach(() => {
        jest.restoreAllMocks();
        fetchMock.restore();
      });

      it('should resolve with response given OK status', async () => {
        const client = new IdentityClient({ baseUrlStargate });
        const userId = 'some-user-id';
        const payloadBody: PatchUserProfileRequestBody = {
          name: 'John Doe',
          nickname: 'jdoe'
        };
        const patchRequest: PatchUserProfileRequest = {
          cloudSessionTokenCookie,
          body: payloadBody,
          userId
        }

        nock(`${baseUrlStargate}`)
        .intercept(`/users/${userId}/manage/profile`, 'PATCH')
        .reply(OK, { account: { name: 'John Doe', nickname: 'jdoe' } });

        await expect(
          client.patchUserProfile(patchRequest)
        ).resolves.toEqual({
          account: {
            name: 'John Doe',
            nickname: 'jdoe'
          }
        });
      });
    });
  });
});
