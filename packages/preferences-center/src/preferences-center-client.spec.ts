import { UnsucessfulFetchError } from '@atlassian/asc-core';
import fetchMock from 'fetch-mock';
import { BAD_REQUEST, INTERNAL_SERVER_ERROR, OK } from 'http-status-codes';
import nock from 'nock';

import { PreferencesCenterClient } from './preferences-center-client';

describe('PreferencesCenterClient', () => {
  const baseUrl = 'http://some-base.url';
  const client = new PreferencesCenterClient({
    baseUrl,
    authTokenProvider: async ({ audience }) => `some-auth-token-for-${audience}`
  });

  describe('getConfig', () => {
    beforeEach(() =>
      fetchMock
        .mock(`${baseUrl}/rest/forms/config?locale=US`, {
          body: {
            locale: 'US',
            localeRequiresMarketingCommunicationOptIn: false
          },
          status: OK
        })
        .mock(`${baseUrl}/rest/forms/config?locale=USA`, {
          body: {
            error: '2 digit country code must be specified.'
          },
          status: INTERNAL_SERVER_ERROR
        })
        .mock(`${baseUrl}/rest/forms/config?locale=`, {
          body: {
            error:
              'locale must be specified either as query param or header from CDN'
          },
          status: BAD_REQUEST
        }));

    afterEach(() => fetchMock.restore());

    it('should resolve with config given valid locale', async () => {
      await expect(client.getConfig({ locale: 'US' })).resolves.toEqual({
        locale: 'US',
        localeRequiresMarketingCommunicationOptIn: false
      });
    });

    it('should be rejected given no locale', async () => {
      await expect(client.getConfig({ locale: '' })).rejects.toThrow(
        UnsucessfulFetchError
      );
    });

    it('should be rejected given invalid locale', async () => {
      await expect(client.getConfig({ locale: 'USA' })).rejects.toThrow(
        UnsucessfulFetchError
      );
    });
  });

  describe('getUserConfig', () => {
    beforeEach(() =>
      fetchMock.post(`${baseUrl}/rest/forms/config`, {
        status: OK,
        body: {}
      }));

    afterEach(() => fetchMock.restore());

    it('should resolve with config given valid locale', async () => {
      await client.getUserConfig({
        email: 'someone@somewhere.org',
        locale: 'US'
      });

      await expect(fetchMock.lastOptions()).toEqual({
        method: 'POST',
        body: JSON.stringify({
          email: 'someone@somewhere.org',
          locale: 'US'
        }),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer some-auth-token-for-atl-preferences-center'
        }
      });
    });
  });

  describe('setUserSubscriptions', () => {
    afterEach(() => nock.cleanAll());

    it('should forward request to downstream service', async () => {
      nock(baseUrl)
        .post('/rest/forms/subscriptions', validRequest)
        .reply(202, { success: true });

      await expect(
        client.setUserSubscriptions(validRequest)
      ).resolves.toBeUndefined();
    });

    const validRequest = {
      email: 'jluong@atlassian.com',
      site: 'atlassian',
      locale: 'US',
      formUrl: 'https://www.atlassian.com/forrester/esm',
      consents: [
        {
          key: 'allowPartnerShare',
          displayedText:
            'By signing up on this form, I agree that Atlassian can share my information with Channel Partners and for Atlassian and Channel Partners to send me promotional emails about products and services.',
          granted: false
        },
        {
          key: 'privacyPolicy',
          displayedText:
            '<p>By&nbsp;signing up on this form, I acknowledge receipt of Atlassian&#39;s <a href=\\"https://www.atlassian.com/legal/privacy-policy\\">Privacy Policy</a>.</p>\n',
          granted: true
        },
        {
          key: 'generalMarketingOptIn',
          displayedText:
            'By signing up on this form, I agree that Atlassian can share my information with Channel Partners and for Atlassian and Channel Partners to send me promotional emails about products and services.',
          granted: false
        }
      ],
      subscriptions: [
        { key: 'atlassian.enterpriseMarketing', subscribed: true }
      ],
      subscriberDetails: {
        firstName: 'Jimmy',
        lastName: 'Luong',
        company: 'Atlassian'
      }
    };
  });
});
