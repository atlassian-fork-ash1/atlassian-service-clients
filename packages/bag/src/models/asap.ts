export type Asap = {
  readonly issuer: string;
  readonly keyId: string;
  readonly privateKey: string;
};
