import { mapResponseToUnsucessfulFetchError } from '@atlassian/asc-core';

export async function mapResponseToText(response: Response): Promise<string> {
  if (response.ok) {
    return await response.text();
  } else {
    throw mapResponseToUnsucessfulFetchError(response);
  }
}

export async function mapResponseToJson(response: Response): Promise<any> {
  if (response.ok) {
    return await response.json();
  } else {
    throw mapResponseToUnsucessfulFetchError(response);
  }
}
